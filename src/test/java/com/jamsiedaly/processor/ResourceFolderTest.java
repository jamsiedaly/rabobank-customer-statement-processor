package com.jamsiedaly.processor;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ResourceFolderTest {

    @Test
    void getResourceFolderFilesReturnsAllFiles() {
        ResourceFolder resourceFolder = new ResourceFolder();
        var resources = resourceFolder.getResourceFolderFiles("test/testRecords");
        assertEquals(3, resources.size());
    }

    @Test
    void getEmptyResourceFolderReturnsNoFiles() {
        ResourceFolder resourceFolder = new ResourceFolder();
        var resources = resourceFolder.getResourceFolderFiles("test/testRecordsEmpty");
        assertEquals(0, resources.size());
    }

    @Test
    void getNonExistentResourceFolderReturnsNoFiles() {
        ResourceFolder resourceFolder = new ResourceFolder();
        var resources = resourceFolder.getResourceFolderFiles("test/madeUpFolder");
        assertEquals(0, resources.size());
    }
}