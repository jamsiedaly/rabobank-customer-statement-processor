package com.jamsiedaly.processor.validator;

import com.jamsiedaly.processor.statement.Statement;
import com.jamsiedaly.processor.statement.StatementError;
import com.jamsiedaly.processor.statement.StatementErrorType;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class StatementValidatorTest {

    private final StatementValidator statementValidator = new StatementValidator();

    private final Statement SAMPLE_STATEMENT = new Statement(
            111111,
            "NL56RABO0149876948",
            "Flowers for Erik Bakker",
            BigDecimal.valueOf(90.68),
            BigDecimal.valueOf(-45.33),
            BigDecimal.valueOf(45.35)
    );

    private final Statement INCORRECT_SAMPLE_STATEMENT = new Statement(
            99999,
            "NL56RABO0149876948",
            "Flowers for Erik Bakker",
            BigDecimal.valueOf(10.00),
            BigDecimal.valueOf(-45.33),
            BigDecimal.valueOf(10.00)
    );

    @Test
    void getStatementDuplicates() {
        List<Statement> statementList = List.of(SAMPLE_STATEMENT, SAMPLE_STATEMENT);
        StatementError duplicateError = new StatementError(SAMPLE_STATEMENT, StatementErrorType.DUPLICATE);

        List<StatementError> errors = statementValidator.getStatementErrors(statementList);

        assertEquals(1, errors.size());
        assertTrue(errors.contains(duplicateError));
    }

    @Test
    void getStatementIncorrectError() {
        List<Statement> statementList = List.of(INCORRECT_SAMPLE_STATEMENT);
        StatementError incorrectBalanceError = new StatementError(INCORRECT_SAMPLE_STATEMENT, StatementErrorType.INCORRECT_BALANCE);

        List<StatementError> errors = statementValidator.getStatementErrors(statementList);

        assertEquals(1, errors.size());
        assertTrue(errors.contains(incorrectBalanceError));
    }
}