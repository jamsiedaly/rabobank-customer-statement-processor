package com.jamsiedaly.processor;

import com.jamsiedaly.processor.statement.Statement;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Path;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class StatementReaderTest {

    private final StatementReader statementReader = new StatementReader();

    private final Statement SAMPLE_STATEMENT = new Statement(
            132843,
            "NL56RABO0149876948",
            "Flowers for Erik Bakker",
            BigDecimal.valueOf(90.68),
            BigDecimal.valueOf(-45.33),
            BigDecimal.valueOf(45.35)
    );

    private final List<String> CSV_EXAMPLE = List.of(
            "Reference,Account Number,Description,Start Balance,Mutation,End Balance",
            "132843,NL56RABO0149876948,Flowers for Erik Bakker,90.68,-45.33,45.35");

    private final List<String> XML_EXAMPLE = List.of(
            "<records>",
            "  <record reference=\"132843\">",
            "    <accountNumber>NL56RABO0149876948</accountNumber>",
            "    <description>Flowers for Erik Bakker</description>",
            "    <startBalance>90.68</startBalance>",
            "    <mutation>-45.33</mutation>",
            "    <endBalance>45.35</endBalance>",
            "  </record>",
            "</records>");

    private Path tempFolder;

    @BeforeEach
    public void setup(@TempDir Path path) throws IOException {
        tempFolder = path;
    }

    @Test
    void getStatementsFromCsvFile() throws IOException {
        final File tempFile = tempFolder.resolve("tempFile.csv").toFile();
        FileUtils.writeLines(tempFile, CSV_EXAMPLE);

        var statements = statementReader.getStatementsFromFile(tempFile);
        var statement = statements.get(0);

        assertTrue(statements.contains(SAMPLE_STATEMENT));
        assertEquals(statement.getAccountNumber(), SAMPLE_STATEMENT.getAccountNumber());
        assertEquals(statement.getDescription(), SAMPLE_STATEMENT.getDescription());
        assertEquals(statement.getMutation(), SAMPLE_STATEMENT.getMutation());
        assertEquals(statement.getStartBalance(), SAMPLE_STATEMENT.getStartBalance());
        assertEquals(statement.getEndBalance(), SAMPLE_STATEMENT.getEndBalance());
    }

    @Test
    void getStatementsFromXmlFile() throws IOException {
        final File tempFile = tempFolder.resolve("tempFile.xml").toFile();
            FileUtils.writeLines(tempFile, XML_EXAMPLE);

        var statements = statementReader.getStatementsFromFile(tempFile);
        var statement = statements.get(0);

        assertTrue(statements.contains(SAMPLE_STATEMENT));
        assertEquals(statement.getAccountNumber(), SAMPLE_STATEMENT.getAccountNumber());
        assertEquals(statement.getDescription(), SAMPLE_STATEMENT.getDescription());
        assertEquals(statement.getMutation(), SAMPLE_STATEMENT.getMutation());
        assertEquals(statement.getStartBalance(), SAMPLE_STATEMENT.getStartBalance());
        assertEquals(statement.getEndBalance(), SAMPLE_STATEMENT.getEndBalance());
    }

    @Test
    void getNoStatementFromEmptyXmlFile() throws IOException {
        final File tempFile = tempFolder.resolve("tempFile.xml").toFile();
        FileUtils.writeLines(tempFile, List.of(""));

        var statements = statementReader.getStatementsFromFile(tempFile);

        assertEquals(0, statements.size());
    }

    @Test
    void getNoStatementFromEmptyCsvFile() throws IOException {
        final File tempFile = tempFolder.resolve("tempFile.csv").toFile();
        FileUtils.writeLines(tempFile, List.of(""));

        var statements = statementReader.getStatementsFromFile(tempFile);

        assertEquals(0, statements.size());
    }

    @Test
    void throwsExceptionForIncorrectFileType() throws IOException {
        final File tempFile = tempFolder.resolve("tempFile.pdf").toFile();

        assertThrows(UnsupportedOperationException.class, () ->
                statementReader.getStatementsFromFile(tempFile));
    }

    @Test
    void throwsExceptionForDeletedFileType() throws IOException {
        final File tempFile = tempFolder.resolve("tempFile.csv").toFile();
        tempFile.delete();

        assertThrows(IOException.class, () ->
                statementReader.getStatementsFromFile(tempFile));
    }
}