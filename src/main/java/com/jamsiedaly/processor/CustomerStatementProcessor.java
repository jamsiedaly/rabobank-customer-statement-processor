package com.jamsiedaly.processor;

import com.jamsiedaly.processor.statement.Statement;
import com.jamsiedaly.processor.statement.StatementError;
import com.jamsiedaly.processor.validator.StatementValidator;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CustomerStatementProcessor {

    private static final Logger logger = Logger.getLogger(CustomerStatementProcessor.class.getName());

    private static final ResourceFolder resources = new ResourceFolder();
    private static final StatementReader reader = new StatementReader();
    private static final StatementReporter reporter = new StatementReporter();
    private static final StatementValidator statementValidator = new StatementValidator();

    public static void main(String[] args) {
        List<File> files = resources.getResourceFolderFiles("records");
        List<Statement> statements = files.stream().flatMap(
                file -> {
                    try {
                        return reader.getStatementsFromFile(file).stream();
                    } catch (UnsupportedOperationException exception) {
                        logger.log(Level.SEVERE, "Unsupported file type", exception);
                        return Stream.empty();
                    } catch (IOException exception) {
                        logger.log(Level.SEVERE, "Failed to read file", exception);
                        return Stream.empty();
                    }
                }
        ).collect(Collectors.toList());
        List<StatementError> statementMistakes = statementValidator.getStatementErrors(statements);
        reporter.publish(statementMistakes);
    }

}