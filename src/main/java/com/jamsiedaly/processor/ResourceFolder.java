package com.jamsiedaly.processor;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ResourceFolder {

    public List<File> getResourceFolderFiles (String folder) {
        File[] filesInDirectory = new File(folder).listFiles();
        if (filesInDirectory == null) return new ArrayList<>();
        else return Arrays.asList(filesInDirectory);
    }
}
