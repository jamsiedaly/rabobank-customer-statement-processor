package com.jamsiedaly.processor;

import com.jamsiedaly.processor.source.IStatementSource;
import com.jamsiedaly.processor.source.StatementSourceFactory;
import com.jamsiedaly.processor.statement.Statement;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class StatementReader {

    public List<Statement> getStatementsFromFile(File file) throws IOException {
        StatementSourceFactory statementFileFactory = new StatementSourceFactory();
        IStatementSource statementFile = statementFileFactory.getSource(file);
        return statementFile.getStatementInputStream().collect(Collectors.toList());
    }
}
