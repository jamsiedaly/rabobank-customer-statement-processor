package com.jamsiedaly.processor;

import com.jamsiedaly.processor.statement.StatementError;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StatementReporter {

    private static final Logger logger = Logger.getLogger(CustomerStatementProcessor.class.getName());

    public void publish(List<StatementError> statementMistakes) {
        statementMistakes.forEach(statement -> {
            logger.log(Level.INFO, statement.toString());
        });
    }
}
