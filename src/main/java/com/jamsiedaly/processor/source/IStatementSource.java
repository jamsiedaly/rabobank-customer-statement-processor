package com.jamsiedaly.processor.source;

import com.jamsiedaly.processor.statement.Statement;

import java.util.stream.Stream;

public interface IStatementSource {
    Stream<Statement> getStatementInputStream();
}
