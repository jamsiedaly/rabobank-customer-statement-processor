package com.jamsiedaly.processor.source;

import com.jamsiedaly.processor.statement.Statement;

import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class CsvStatementSource implements IStatementSource {

    private final List<Statement> statements = new ArrayList<>();

    public CsvStatementSource(File file) throws IOException {
        try (
            InputStream inputFS = new FileInputStream(file);
            BufferedReader br = new BufferedReader(new InputStreamReader(inputFS))
        ) {
            br.lines().skip(1).forEach(line -> {
                String[] info = line.split(",");
                statements.add(
                        new Statement(
                                Integer.parseInt(info[0]),
                                info[1],
                                info[2],
                                new BigDecimal(info[3]),
                                new BigDecimal(info[4]),
                                new BigDecimal(info[5])
                        )
                );
            });
        }
    }

    @Override
    public Stream<Statement> getStatementInputStream() {
        return statements.stream();
    }
}
