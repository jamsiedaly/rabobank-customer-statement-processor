package com.jamsiedaly.processor.source;

import java.io.File;
import java.io.IOException;

public class StatementSourceFactory {

    public IStatementSource getSource(File file) throws IOException {
        if (file.getName().endsWith(".csv")) {
            return new CsvStatementSource(file);
        } else if(file.getName().endsWith(".xml")) {
            return new XmlStatementSource(file);
        } else {
            throw new UnsupportedOperationException("File type not supported: " + file);
        }
    }
}
