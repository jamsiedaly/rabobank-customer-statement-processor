package com.jamsiedaly.processor.source;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.jamsiedaly.processor.statement.Statement;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

public class XmlStatementSource implements IStatementSource {

    List<Statement> statements;

    public XmlStatementSource(File file) throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        String xml = inputStreamToString(new FileInputStream(file));
        try {
            statements = Arrays.asList(xmlMapper.readValue(xml, Statement[].class));
        } catch (JsonParseException exception) {
            statements = Collections.emptyList();
        }
    }

    private String inputStreamToString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        String line;
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        br.close();
        return sb.toString();
    }

    @Override
    public Stream<Statement> getStatementInputStream() {
        return statements.stream();
    }
}
