package com.jamsiedaly.processor.validator;

import com.jamsiedaly.processor.statement.Statement;
import com.jamsiedaly.processor.statement.StatementError;
import com.jamsiedaly.processor.statement.StatementErrorType;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.function.UnaryOperator.identity;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

public class StatementValidator {

    public List<StatementError> getStatementErrors(List<Statement> statements) {
        Stream<StatementError> duplicates = getDuplicates(statements);
        Stream<StatementError> incorrectStatements = getIncorrectStatements(statements);
        var statementMistakes = Stream.concat(duplicates, incorrectStatements);
        return statementMistakes.collect(Collectors.toList());
    }

    private Stream<StatementError> getIncorrectStatements(List<Statement> statements) {
        return statements.stream().filter(statement -> {
            var endBalance = statement.getEndBalance();
            var startBalance = statement.getStartBalance();
            var mutation = statement.getMutation();
            return !endBalance.equals(startBalance.add(mutation));
        }).map(statement -> new StatementError(
                statement,
                StatementErrorType.INCORRECT_BALANCE
        ));
    }

    private Stream<StatementError> getDuplicates(List<Statement> statements) {
        return statements.stream().collect(
                groupingBy(identity(), counting()))
                .entrySet().stream()
                .filter(occurrences -> occurrences.getValue() > 1)
                .map(Map.Entry::getKey)
                .map(statement -> new StatementError(
                        statement,
                        StatementErrorType.DUPLICATE
                ));
    }
}
