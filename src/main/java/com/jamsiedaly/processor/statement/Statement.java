package com.jamsiedaly.processor.statement;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Objects;

@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Statement {

    @Getter private int reference;
    @Getter private String accountNumber;
    @Getter private String description;
    @Getter private BigDecimal startBalance;
    @Getter private BigDecimal mutation;
    @Getter private BigDecimal endBalance;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Statement statement = (Statement) o;
        return reference == statement.reference;
    }

    @Override
    public int hashCode() {
        return Objects.hash(reference);
    }
}
