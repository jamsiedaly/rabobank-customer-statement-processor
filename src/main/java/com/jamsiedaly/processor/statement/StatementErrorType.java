package com.jamsiedaly.processor.statement;

public enum StatementErrorType {
    DUPLICATE,
    INCORRECT_BALANCE
}
