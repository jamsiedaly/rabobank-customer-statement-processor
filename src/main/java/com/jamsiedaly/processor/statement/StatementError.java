package com.jamsiedaly.processor.statement;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Objects;

@AllArgsConstructor
@Data
public class StatementError {
    private final Statement statement;
    private final StatementErrorType error;
}
