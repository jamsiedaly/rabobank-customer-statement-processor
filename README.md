# Customer Statement Processor
Implemented by James Daly of Code Nomads.

[![pipeline status](https://gitlab.com/jamsiedaly/rabobank-customer-statement-processor/badges/master/pipeline.svg)](https://gitlab.com/jamsiedaly/rabobank-customer-statement-processor/-/commits/master)
[![coverage report](https://gitlab.com/jamsiedaly/rabobank-customer-statement-processor/badges/master/coverage.svg)](https://gitlab.com/jamsiedaly/rabobank-customer-statement-processor/-/commits/master)

## Implementation
This application is built using:
* Java 11
* Maven
* Junit 5
* Jackson

When creating this application I decided to try keep things relatively simple.
I did not use a large framework such as Spring or Quarkus.
This was to keep the applications footprint light and to make it easier for a reviewer to navigate. 

## Running Locally
When the application is run it will output the statements which are not valid along with the reason.
```
mvn package
java -jar target/CustomerStatementProcessor-1.0-SNAPSHOT-jar-with-dependencies.jar
```

## Running Tests Locally
```
mvn test
```

## Inspecting With Sonarqube 
```
docker pull sonarqube
docker run -d --name sonarqube -p 9000:9000 sonarqube
```
You can now log into a local instance of [sonarqube](http://localhost:9000).

| Username | Password |
| ---------|---------:|
| admin    | admin    |

Here you can create a project and you will be given an auth toke for your local instance.
Now you can analyze the project.

```
mvn sonar:sonar \
  -Dsonar.projectKey={projectName}} \
  -Dsonar.host.url=http://localhost:9000 \
  -Dsonar.login={projectToken}} \
  -Dsonar.java.source=11 \
  -Dsonar.jacoco.reportPath=target/jacoco.exe
```


